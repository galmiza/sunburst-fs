/*
 * Json manipulation
 * - getchild
 * - getchildrec
 * - deepcopy
 * - csv2json
 */
 
// Json hierarchy manipulation
var getchild = function(obj,childname) {
  for (var i=0,n=obj.children.length;i<n;i++) {
    if (obj.children[i].nodeName==childname)
      return obj.children[i];
  }
}

var getchildrec = function(obj,path) {
  var path = deepcopy(path);
  var child = getchild(obj,path.shift());
  if (path.length!=0 && child)
    return getchildrec(child,path);
  return child;
}

// Json deep copy
var deepcopy = function(json) {
  return JSON.parse(JSON.stringify(json));
}

// Conversion of a flat structure to Json hierarchy
var csv2json = function(data,mode) { // mode: count|sum
  var root = {children:[],nodeName:"root"};
  var lines = data.split("\n");
  lines.map(function(l) {
    try {
      var ls = l.split("\t");
      var size = parseInt(ls[0]);
      var path = ls[1];
      subpath = path.split("/");
      subpath.shift(); // get rid of the leading empty element
      filename = subpath.pop().trim();
      var r = root; // reference
      for (var i=0,n=subpath.length;i<n;i++) {
        var child = getchild(r,subpath[i]);
        if (!child) {
          child = {nodeName:subpath[i],children:[]}
          r.children.push(child);
        }
        r = child;
      }
      r.children.push({nodeName:filename,size:mode=='count'?1:size})
    } catch(e) {
      console.log(e);
    }
  });
  return root;
}