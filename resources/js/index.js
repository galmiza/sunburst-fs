//
// Handle options in banner (load file structure, aggregation type sum/count)
// Handle hash change (hash=path in file structure to render)
// Render sunburst from hierarchical json
// Handle mouse events
//

/*
 * Handle options in the banner
 * + file change (paperclip icon) to load a file structure
 * + mode change (drop-down box) to switch between 'sum' and 'count'
 */
var csv,json;
d3.select("#finput").node().onchange = function(e) {
  var file = e.target.files[0];
  if (!file) return;
  var reader = new FileReader();
  reader.onload = function(e) {
    csv = e.target.result;
    var mode = d3.select("#mode").node().value;
    json = csv2json(csv,mode);
    clearPath();
    updateSunburst();
  };
  reader.readAsText(file);
}
d3.select("#mode").node().onchange = function() {
  var mode = d3.select("#mode").node().value;
  json = csv2json(csv,mode);
  updateSunburst();
}
var selectFile = function() {
  d3.select("#finput").node().click();
}


/*
 * Handle hash changes in the url
 * The hash represents the path of the folder in the file structure to render
 * On hash change, the sunburnt is drawn
 */
var currentPath = [];
window.onhashchange = function() {
  var h = decodeURIComponent(window.location.hash.slice(2)); // slice to remove char #/
  currentPath = h.split("/");
  if (h=="") currentPath = [];
  //console.log("onhashchange",currentPath);
  updateSunburst();
};
var clearPath = function() {
  window.location.hash = "";
}
var updatePath = function(relativePath) {
  currentPath = currentPath.concat(relativePath);
  window.location.hash = "/"+currentPath.join("/");
}


/*
 * Sunburst rendering using d3js
 * Source: https://bl.ocks.org/kerryrodden/7090426
 */
 
// Global variable used in mouse events
var vis;
var totalSize = 0;

// Render sunburst from a hierarchical json
function createVisualization(json) {

  var width = $(window).width();
  var height = $(window).height()-$(".banner").height();
  var radius = Math.min(width, height) / 2;
  var partition = d3.partition().size([2 * Math.PI, radius * radius]);
  var arc = d3.arc()
      .startAngle(function(d) { return d.x0; })
      .endAngle(function(d) { return d.x1; })
      .innerRadius(function(d) { return Math.sqrt(d.y0); })
      .outerRadius(function(d) { return Math.sqrt(d.y1); });

  // Clear previous chart
  d3.select("#chart").text("");

  vis = d3.select("#chart").append("svg:svg")
    .attr("width", width)
    .attr("height", height)
    .append("svg:g")
    .attr("id", "container")
    .attr("transform", "translate(" + width/2 + "," + height/2 + ")");
    
  // Bounding circle underneath the sunburst, to make it easier to detect
  // when the mouse leaves the parent g.
  vis.append("svg:circle")
      .attr("r", radius)
      .attr("id", "sunburst")
      .style("opacity", 0);

  // Turn the data into a d3 hierarchy and calculate the sums.
  var root = d3.hierarchy(json)
      .sum(function(d) { return d.size; })
      .sort(function(a, b) { return b.value - a.value; });
  
  // For efficiency, filter nodes to keep only those large enough to see.
  var nodes = partition(root).descendants()
      .filter(function(d) {
          return (d.x1 - d.x0 > 0.005); // 0.005 radians = 0.29 degrees
      });

  var path = vis.data([json]).selectAll("path")
      .data(nodes)
      .enter().append("svg:path")
      .attr("display", function(d) { return d.depth ? null : "none"; })
      .attr("d", arc)
      .attr("fill-rule", "evenodd")
      .style("fill", function(d) { return "#9120a5"; })
      .style("opacity", 1)
      .on("click", mouseclick)
      .on("mouseover", mouseover);

  // Add the mouseleave handler to the bounding circle.
  d3.select("#container").on("mouseleave", mouseleave);

  // Get total size of the tree = value of root node from partition.
  totalSize = path.datum().value;
 };

// Trigger rendering at currentPath
var updateSunburst = function() {
  if (currentPath.length!=0) createVisualization(getchildrec(json,currentPath));
  else                       createVisualization(json);
}
window.onresize = updateSunburst;


/*
 * Handle mouse events
 * + mouseclick, browse file structure and redraw
 * + mouseover, highlight current path in sunburst
 * + mouseleave, make sunburst fully opaque
 */
 
// Update path to render and redraw
var mouseclick = function(d) {
  var sequenceArray = d.ancestors().reverse();
  var path = sequenceArray.map(function(i) { return i.data.nodeName;}).slice(1);
  updatePath(path);
}

// Fade all sunburst arcs but those from the overflown element
var mouseover = function(d) {
  var sequenceArray = d.ancestors().reverse().slice(1);
  var path = "/"+currentPath.concat(sequenceArray.map(function(i) { return i.data.nodeName;})).join("/");
  
  // Update path and details of the overflown element
  d3.select("#path").text(path);
  d3.select("#details").text(d.value+" / "+totalSize+" ("+(Math.floor(10000*d.value/totalSize)/100)+"%)");

  // Fade all the segments, highlight only those that are an ancestor of the current segment
  d3.selectAll("path").style("opacity", 0.3);
  vis.selectAll("path")
      .filter(function(node) { return (sequenceArray.indexOf(node) >= 0); })
      .style("opacity", 1);
}

// Restore everything to full opacity when moving off the visualization.
var mouseleave = function(d) {

  // Deactivate all segments during transition.
  d3.selectAll("path").on("mouseover", null);

  // Transition each segment to full opacity and then reactivate it.
  d3.selectAll("path")
      .transition()
      .duration(100)
      .style("opacity", 1)
      .on("end", function() { d3.select(this).on("mouseover", mouseover); });
}

