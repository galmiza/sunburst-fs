# Welcome

This web application renders a **file structure as a sunburst**.  
The sunburst representation lets you to visualize the **distribution of the number of files or size of files** in a deep file structure.

![img](../../downloads/Screenshot%202019-02-06%2009.24.23.png)

Note that the implementation of the sunburst rendering is totally inspired from https://bl.ocks.org/kerryrodden/7090426

# Use case

A sunburst representation is a perfect tool to help **cleaning a disk** in order to quickly **reduce the number of file** or simply **reduce the disk usage**. 


# Install

Clone the repository into your web server root directory, that's it.

```
cd /var/www/html
sudo git clone https://galmiza@bitbucket.org/galmiza/sunburst-fs.git
```

# Usage

* Open the application in your browser
* Click on the paperclip icon
* Select your file structure (csv, see below)

# File structure input format

The file structure input should be a csv where each line represents a **path** and a **size** in bytes.

* The first column represents the size
* The second column represents the path

Columns are separated with the char \t.

_Example :_

```
33084	/volume1/data/projects/foo.zip
77554	/volume1/data/projects/bar/foo.txt
113806	/volume1/photo/album1/pic01.jpg
93235	/volume1/photo/album1/pic02.jpg
67900	/volume1/photo/album1/pic03.jpg
3634	/volume1/private/passwords.kdbx
33871	/volume1/foo.txt
```

On most unix systems, you can generate the file structure using the command below.

```
find /volume1/data -type f -exec du -b {} \; >data.csv
```

# Options

Using the drop down menu at the top right of the page, you can switch between :

* rendering the distribution of the number of files (count)
* rendering the distribution of the size of files (sum)

# Demo

Check the demo here :  
https://www.galmiza.net/public/sunburst-fs